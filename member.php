<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Bitcoin Pro App</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="noindex, nofollow" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css"/>
        <link rel="stylesheet" href="css/media.css"/>
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <link rel="icon" href="favicon.ico" type="image/x-icon">
    </head>
    <body>
        <div class="heading-block heading-customize">
            <nav class="navbar navbar-inverse">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>                        
                        </button>
                        <a class="navbar-brand" href="index.php">
                            <img src="images/bitcoinpro-logo.png" alt="Bitcoin Pro Logo">
                        </a>
                    </div>
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="#">Home</a></li>
                            <li><a href="#">Section</a></li>
                            <li><a href="#">Section</a></li>
                            <li><a href="#">Section</a></li>
                            <li><a href="#">Section</a></li>
                            <li><a href="#">Section</a></li>
                        </ul>
                    </div>
                    <div class="notification-block">
                        <img src="images/account-image.png" alt="Bitcoin Pro">
                        <div class="transact-desc">
                            <span>Just Made</span><br>
                            <span class="transact-amt">$231</span>
                        </div>
                    </div>
                </div>
            </nav>

            <div class="heading-content">
                <div class="container text-center">
                    <h1 class="title-heading title-gradient">The World's #1 Crypto Profits System</h1>
                    <div class="row wrapper-gutter">
                        <div class="col-md-8">
                            <img class="video-clip" src="images/video.jpg" alt="Bitcoin Crypto">
                        </div>
                        <div class="col-md-4 text-center">
                            <div class="form-wrapper">
                                <div class="form-block">
                                    <div class="content-block">
                                        <div class="line-wrap">
                                            <ul>
                                                <li>1</li>
                                                <li class="active">2</li>
                                                <li>3</li>
                                            </ul>
                                        </div>
                                        <form id="step2-form-a"  class="crypto signup-form">
                                            <fieldset>
                                                <div class="row name-block group-form">
                                                    <div class="col-xs-6 col-md-6 col-md-6 no-gutter">
                                                        <input type="text" name="fname" placeholder="First Name" class="f-name" required>
                                                    </div>
                                                    <div class="col-xs-6 col-md-6 col-md-6 no-gutter"> 
                                                        <input type="text" name="lname" placeholder="Last Name" class="l-name" required>
                                                    </div>
                                                </div>
                                                <div class="group-form">
                                                    <input type="email" name="email" placeholder="Your Email" class="email" required>
                                                </div>
                                                <div class="group-form">
                                                    <input class="prefix" name="prefix" type="text" readonly>
                                                    <input type="text" name="phone" placeholder="Phone Number" class="phone" required>
                                                </div>
                                                <div class="group-form">
                                                    <input type="password" name="password" placeholder="Create Password" class="password" required>
                                                </div>
                                                <div class="btn-block-01">
                                                    <div class="btn-block-02">
                                                        <input type="submit" name="submit" value="Next Step" class="next-step btn">
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
        
        <div class="block_08">
            <div class="container-fluid">
                <div class="block-wrap">
                    <div class="block-content">
                        <p class="text-uppercase">Jan 4, 2018 | Bitcoin Pro App</p>
                        <h2 class="h1 text-white text-uppercase">High Profiting & Free Crypto System</h2>
                        <p class="text-white mb-30">With the recent increase of global interest in the Crypto currency Trading World, new traders and new crypto users, have the complicated task of selecting the correct tools for them. Here at CryptoEdge, e offer all our users free
                            tools to help them profit big in the lucrative age of Crypto currencies.
                        </p>
                        <p>Rest assured that all you ever wanted to achieve from Crypto Trading, is at the tip of your fingers. Now you have the opportunity to be one step ahead of even the biggest traders in the world!
                        </p>
                    </div>
                </div>
                <div class="block-image no-gutter">
                    <img class="img-responsive" src="images/coin.jpg" alt="Bitcoin Pro Coin" />
                </div>
            </div>
        </div>
        
        <div class="block_09">
            <div class="container-fluid">
                <div class="block-image no-gutter">
                    <img class="img-responsive" src="images/people.jpg" alt="Bitcoin Pro People" />
                </div>
                    
                <div class="block-wrap">
                    <div class="block-content">
                        <h3 class="text-uppercase">Our Mission</h3>
                        <p class="first">The world is leaping forward with new technologies & new industries. We are here to make sure you make your step as well but in the right direction.</p>
                        <p>The Crypto Edge System includes an incredible FREE E-book and Crypto Trading Software that you receive free! All planned to insure an exciting future of profits and opportunities as our CryptoEdge Exclusive Member.</p>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="block_010">
            <div class="container-fluid">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                    <li data-target="#myCarousel" data-slide-to="2"></li>
                </ol>
                <div class="container">
                    <h3 class="text-center">What Our Clients Are Saying</h3>
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="row">
                                <div class="col-md-3">
                                    <em class="flag-01"></em>
                                    <span class="name">Dewitt Parker</span>
                                    <span class="comment">“Bitcoin Pro is the only service I can trust when it comes to such a complicated subject like crypto currencies!”</span>
                                </div>
                                <div class="col-md-3">
                                    <em class="flag-02"></em>
                                    <span class="name">Adam Dame</span>
                                    <span class="comment">I’ve trade so many Crypto services but by far this is the most impressive one.</span>
                                </div>
                                <div class="col-md-3">
                                    <em class="flag-03"></em>
                                    <span class="name">Jenna Morais</span>
                                    <span class="comment">Your support team was very welcoming, helpful. In one day, they showed me how to make $783 in profit and the shocking part… all for free!</span>
                                </div>
                                <div class="col-md-3">
                                    <em class="flag-04"></em>
                                    <span class="name">Robert Caldwell</span>
                                    <span class="comment">I’m loving the high Bitcoin profits I’m making since I started with CryptoEdge Literally, the best move I have ever made</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <em class="flag-01"></em>
                                    <span class="name">Dewitt Parker</span>
                                    <span class="comment">“Bitcoin Pro is the only service I can trust when it comes to such a complicated subject like crypto currencies!”</span>
                                </div>
                                <div class="col-md-3">
                                    <em class="flag-02"></em>
                                    <span class="name">Adam Dame</span>
                                    <span class="comment">I’ve trade so many Crypto services but by far this is the most impressive one.</span>
                                </div>
                                <div class="col-md-3">
                                    <em class="flag-03"></em>
                                    <span class="name">Jenna Morais</span>
                                    <span class="comment">Your support team was very welcoming, helpful. In one day, they showed me how to make $783 in profit and the shocking part… all for free!</span>
                                </div>
                                <div class="col-md-3">
                                    <em class="flag-04"></em>
                                    <span class="name">Robert Caldwell</span>
                                    <span class="comment">I’m loving the high Bitcoin profits I’m making since I started with CryptoEdge Literally, the best move I have ever made</span>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-md-3">
                                    <em class="flag-01"></em>
                                    <span class="name">Dewitt Parker</span>
                                    <span class="comment">“Bitcoin Pro is the only service I can trust when it comes to such a complicated subject like crypto currencies!”</span>
                                </div>
                                <div class="col-md-3">
                                    <em class="flag-02"></em>
                                    <span class="name">Adam Dame</span>
                                    <span class="comment">I’ve trade so many Crypto services but by far this is the most impressive one.</span>
                                </div>
                                <div class="col-md-3">
                                    <em class="flag-03"></em>
                                    <span class="name">Jenna Morais</span>
                                    <span class="comment">Your support team was very welcoming, helpful. In one day, they showed me how to make $783 in profit and the shocking part… all for free!</span>
                                </div>
                                <div class="col-md-3">
                                    <em class="flag-04"></em>
                                    <span class="name">Robert Caldwell</span>
                                    <span class="comment">I’m loving the high Bitcoin profits I’m making since I started with CryptoEdge Literally, the best move I have ever made</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <em class="flag-01"></em>
                                    <span class="name">Dewitt Parker</span>
                                    <span class="comment">“Bitcoin Pro is the only service I can trust when it comes to such a complicated subject like crypto currencies!”</span>
                                </div>
                                <div class="col-md-3">
                                    <em class="flag-02"></em>
                                    <span class="name">Adam Dame</span>
                                    <span class="comment">I’ve trade so many Crypto services but by far this is the most impressive one.</span>
                                </div>
                                <div class="col-md-3">
                                    <em class="flag-03"></em>
                                    <span class="name">Jenna Morais</span>
                                    <span class="comment">Your support team was very welcoming, helpful. In one day, they showed me how to make $783 in profit and the shocking part… all for free!</span>
                                </div>
                                <div class="col-md-3">
                                    <em class="flag-04"></em>
                                    <span class="name">Robert Caldwell</span>
                                    <span class="comment">I’m loving the high Bitcoin profits I’m making since I started with CryptoEdge Literally, the best move I have ever made</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="arrow-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="arrow-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            </div>
        </div>
        
        <div class="block_11">
            <div class="container">
                <div class="col-container-1">
                    <div class="col-image">
                        <img class="images" src="images/fafa.png" alt="coin">
                    </div>
                    
                    <div class="col-text">
                        <div>
                            <h3 class="text-uppercase">Hot Updates</h3>
                            <p>Notification on HOT NEWs in the Crypto & ICO World. High valued Tips & News delivered to you, daily, related to all crypto subjects.</p>
                        </div>
                    </div>
                </div>
                
                <div class="col-container-2">
                    <div class="col-image">
                            <img class="images" src="images/tv.png" alt="coin">
                    </div>
                    <div class="col-text">
                        <div>
                            <h3 class="text-uppercase">Extra Guidance</h3>
                            <p>More educational tools related to Blockchain & other interesting crypto subjects which we offer at no cost with no terms.
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="heading-block heading-customize">
            <div class="heading-content">
                <div class="container text-center">
                    <h1 class="title-heading title-gradient">The World's #1 Crypto Profits System</h1>
                    <div class="row wrapper-gutter">
                        <div class="col-md-8">
                            <img class="video-clip" src="images/video.jpg" alt="Bitcoin Crypto">
                        </div>
                        <div class="col-md-4 text-center">
                            <div class="form-wrapper">
                                <div class="form-block">
                                    <div class="content-block">
                                        <div class="line-wrap">
                                            <ul>
                                                <li>1</li>
                                                <li class="active">2</li>
                                                <li>3</li>
                                            </ul>
                                        </div>
                                        <form id="step2-form-b"  class="crypto signup-form">
                                            <fieldset>
                                                <div class="row name-block group-form">
                                                    <div class="col-xs-6 col-md-6 col-md-6 no-gutter">
                                                        <input type="text" name="fname" placeholder="First Name" class="f-name" required>
                                                    </div>
                                                    <div class="col-xs-6 col-md-6 col-md-6 no-gutter"> 
                                                        <input type="text" name="lname" placeholder="Last Name" class="l-name" required>
                                                    </div>
                                                </div>
                                                <div class="group-form">
                                                    <input type="email" name="email" placeholder="Your Email" class="email" required>
                                                </div>
                                                <div class="group-form">
                                                    <input class="prefix" name="prefix" type="text" readonly>
                                                    <input type="text" name="phone" placeholder="Phone Number" class="phone" required>
                                                </div>
                                                <div class="group-form">
                                                    <input type="password" name="password" placeholder="Create Password" class="password" required>
                                                </div>
                                                <div class="btn-block-01">
                                                    <div class="btn-block-02">
                                                        <input type="submit" name="submit" value="Next Step" class="next-step">
                                                    </div>
                                                </div>
                                            </fieldset>
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>  
        </div>
        <footer>
            <div class="container">
                <p class="text-center text-white">© 2018 All Right Reserved</p>
            </div>
        </footer>
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
        
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        
        <!-- jQuery Validate -->
        <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>

        <!--geoplugin -->
        <script src="http://www.geoplugin.net/javascript.gp"></script>
        
        <!-- jQuery libphonenumber -->
        <script src="js/libphonenumber.js"></script>
        
        <!-- Customized JS-->
        <script src="js/main.js"></script>
    </body>
</html>