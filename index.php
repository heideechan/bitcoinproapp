<!DOCTYPE html>
<html lang="en">
    <head>
        <title>Bitcoin Pro App</title>
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="robots" content="noindex, nofollow" />
        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,600,800" rel="stylesheet">
        <link rel="stylesheet" href="css/style.css"/>
        <link rel="stylesheet" href="css/media.css"/>
        <link rel="shortcut icon" href="favicon.ico" type="image/x-icon">
        <link rel="icon" href="favicon.ico" type="image/x-icon">
    </head>
    <body>
        <div class="heading-block">
            <nav class="navbar navbar-inverse">
                <div class="container">
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle btn" data-toggle="collapse" data-target="#myNavbar">
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>
                            <span class="icon-bar"></span>                        
                        </button>
                        <a class="navbar-brand" href="index.php">
                            <img src="images/bitcoinpro-logo.png" alt="Bitcoin Pro Logo" />
                        </a>
                    </div>
                    <div class="collapse navbar-collapse" id="myNavbar">
                        <ul class="nav navbar-nav">
                            <li class="active"><a href="#">Home</a></li>
                            <li><a href="#">Section</a></li>
                            <li><a href="#">Section</a></li>
                            <li><a href="#">Section</a></li>
                            <li><a href="#">Section</a></li>
                            <li><a href="#">Section</a></li>
                        </ul>
                    </div> 
                </div>
            </nav>
            <div class="notification-block">
                <img src="images/account-image.png" alt="Bitcoin Pro">
                <div class="transact-desc">
                    <span>Just Made</span><br>
                    <span class="transact-amt">$231</span>
                </div>
            </div>
            <div class="heading-content">
                <div class="container text-center">
                    <h1 class="title-heading title-gradient">The World's #1 Crypto Profits System</h1>
                    <div class="row wrapper-gutter">
                        <div class="col-md-8">
                            <img class="video-clip" src="images/video.jpg" alt="Bitcoin Pro Video" />
                        </div>
                   
                        <div class="col-md-4 text-center">
                            <div class="form-wrapper">
                                    <div class="text-block">
                                        <span>Start Changing</span>
                                        <span class="orange">Your Life Today</span>
                                    </div>
                                    <div class="form-block">
                                        <div class="content-block">
                                            <div class="line-wrap">
                                                <ul>
                                                    <li class="active">1</li>
                                                    <li>2</li>
                                                    <li>3</li>
                                                </ul>
                                            </div>
                                            <form id="step1-form-a"  class="crypto signup-form">
                                                <fieldset>
                                                    <div class="group-form">
                                                        <input type="text" name="fullname" placeholder="Your full name" class="name" required>
                                                    </div>
                                                    <div class="group-form">
                                                        <input type="email" name="email" placeholder="Your Email" class="email">
                                                    </div>
                                                    <div class="btn-block-01">
                                                        <div class="btn-block-02">
                                                            <input type="submit" name="submit" value="Next Step" class="next-step">
                                                        </div>
                                                    </div>
                                                </fieldset>
                                            </form>
                                        </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>    
        
        
        <div class="block_01">
            <div class="container-fluid">
                    <div class="block-wrap">
                        <div class="block-content">
                            <h3>Title Here</h3>
                            <p>Lorem ipsum bla bla The world is leaping forward with new technologies & new industries. 
                                We are here to make sure you make your step as well but in the right direction.</p>
                            <ul class="coins-list">
                                <li>
                                    <img src="images/monero_coin.png" alt="Monero Coin" />
                                    <div class="coins-description">
                                        <span>Monero</span>
                                        <span>(XMR)</span>
                                    </div>
                                </li>
                                <li>
                                    <img src="images/dash_coin.png" alt="Dash Coin" />
                                    <div class="coins-description">
                                        <span>Dash</span>
                                        <span>(DASH)</span>
                                    </div>
                                </li>
                                <li>
                                    <img src="images/name_coin.png" alt="Name Coin" />
                                    <div class="coins-description">
                                        <span>Namecoin</span>
                                        <span>(NMC)</span>
                                    </div>
                                </li>
                                <li>
                                    <img src="images/peer_coin.png" alt="Peer Coin" />
                                    <div class="coins-description">
                                        <span>Peercoin</span>
                                        <span>(PPC)</span>
                                    </div>
                                </li>
                                <li>
                                    <img src="images/bitcash_coin.png" alt="Bitcash Coin" />
                                    <div class="coins-description">
                                        <span>Bitcoin cash</span>
                                        <span>(BCH)</span>
                                    </div>
                                </li>
                                <li>
                                    <img src="images/world_coin.png" alt="World Coin" />
                                    <div class="coins-description">
                                        <span class="position-adjust">Worldcoin</span>
                                        <span>(WDC)</span>
                                    </div>
                                </li>
                                <li>
                                    <img src="images/lite_coin.png" alt="Lite Coin" />
                                    <div class="coins-description">
                                        <span>Litecoin</span>
                                        <span>(LTC)</span>
                                    </div>
                                </li>
                                <li>
                                    <img src="images/bit_coin.png" alt="Bitcoin" />
                                    <div class="coins-description">
                                        <span>Bitcoin</span>
                                        <span>(BTC)</span>
                                    </div>
                                </li>
                                <li>
                                    <img src="images/ethereum.png" alt="Ethereum" />
                                    <div class="coins-description">
                                        <span>Ethereum</span>
                                        <span>(ETH)</span>
                                    </div>
                                </li>
                                <li>
                                    <img src="images/neo.png" alt="Neo Coin" />
                                    <div class="coins-description">
                                        <span>Neo</span>
                                        <span>(NEO)</span>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                    
                    <div class="no-gutter block-image">
                        <img class="img-responsive" src="images/image_02.jpg" alt="Bitcoin Pro app" />
                    </div>
            </div>
        </div>
        
        <!-- ./block_01 -->
        
        <div class="block_02">
            <div class="container-fluid">
                    <div class="block-wrap">
                        <div class="block-content">
                            <h3>High Profile & Free Crypto Trading System</h3>
                                <p>With the recent increase of global interest in the Cryptocurrency Trading World, new traders and new crypto users, have the complicated task of selecting the correct tools for them. </p>
                                <p>Here at CryptoEdge, we offer all our users free tools to help them profit big in the lucrative age of Cryptocurrencies. Rest assured that all you ever wanted to achieve from Crypto Trading, is at the tip of your fingers. Now you have the opportunity to be one step ahead of even the biggest traders in the world!</p>
                                <div class="line-bar">
                                    <div class="bar-progress bar-1">
                                        <span>Arbitrage Strategies</span>
                                    </div>
                                    <div class="bar-progress bar-2">
                                        <span>High Quality Code</span>
                                    </div>
                                    <div class="bar-progress bar-3">
                                        <span>Healthy Profit Model</span>
                                    </div>
                                </div>                        
                        </div>
                    </div>
                    <div class="block-image no-gutter">
                        <img class="img-responsive" src="images/graph.png" alt="Bitcoin Pro Dashboard" />
                    </div>
            </div>
        </div>
        <div class="block_03">
            <div class="container-fluid">
                <div id="myCarousel" class="carousel slide" data-ride="carousel">
                <!-- Indicators -->
                <ol class="carousel-indicators">
                    <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                    <li data-target="#myCarousel" data-slide-to="1"></li>
                </ol>
                <div class="container">
                    <h3 class="text-center">What Our Clients Are Saying</h3>
                    <div class="carousel-inner">
                        <div class="item active">
                            <div class="row">
                                <div class="col-md-3">
                                    <em class="flag-01"></em>
                                    <span class="name">Dewitt Parker</span>
                                    <span class="comment">“Bitcoin Pro is the only service I can trust when it comes to such a complicated subject like crypto currencies!”</span>
                                </div>
                                <div class="col-md-3">
                                    <em class="flag-02"></em>
                                    <span class="name">Adam Dame</span>
                                    <span class="comment">I’ve trade so many Crypto services but by far this is the most impressive one.</span>
                                </div>
                                <div class="col-md-3">
                                    <em class="flag-03"></em>
                                    <span class="name">Jenna Morais</span>
                                    <span class="comment">Your support team was very welcoming, helpful. In one day, they showed me how to make $783 in profit and the shocking part… all for free!</span>
                                </div>
                                <div class="col-md-3">
                                    <em class="flag-04"></em>
                                    <span class="name">Robert Caldwell</span>
                                    <span class="comment">I’m loving the high Bitcoin profits I’m making since I started with CryptoEdge Literally, the best move I have ever made</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <em class="flag-01"></em>
                                    <span class="name">Dewitt Parker</span>
                                    <span class="comment">“Bitcoin Pro is the only service I can trust when it comes to such a complicated subject like crypto currencies!”</span>
                                </div>
                                <div class="col-md-3">
                                    <em class="flag-02"></em>
                                    <span class="name">Adam Dame</span>
                                    <span class="comment">I’ve trade so many Crypto services but by far this is the most impressive one.</span>
                                </div>
                                <div class="col-md-3">
                                    <em class="flag-03"></em>
                                    <span class="name">Jenna Morais</span>
                                    <span class="comment">Your support team was very welcoming, helpful. In one day, they showed me how to make $783 in profit and the shocking part… all for free!</span>
                                </div>
                                <div class="col-md-3">
                                    <em class="flag-04"></em>
                                    <span class="name">Robert Caldwell</span>
                                    <span class="comment">I’m loving the high Bitcoin profits I’m making since I started with CryptoEdge Literally, the best move I have ever made</span>
                                </div>
                            </div>
                        </div>
                        <div class="item">
                            <div class="row">
                                <div class="col-md-3">
                                    <em class="flag-01"></em>
                                    <span class="name">Dewitt Parker</span>
                                    <span class="comment">“Bitcoin Pro is the only service I can trust when it comes to such a complicated subject like crypto currencies!”</span>
                                </div>
                                <div class="col-md-3">
                                    <em class="flag-02"></em>
                                    <span class="name">Adam Dame</span>
                                    <span class="comment">I’ve trade so many Crypto services but by far this is the most impressive one.</span>
                                </div>
                                <div class="col-md-3">
                                    <em class="flag-03"></em>
                                    <span class="name">Jenna Morais</span>
                                    <span class="comment">Your support team was very welcoming, helpful. In one day, they showed me how to make $783 in profit and the shocking part… all for free!</span>
                                </div>
                                <div class="col-md-3">
                                    <em class="flag-04"></em>
                                    <span class="name">Robert Caldwell</span>
                                    <span class="comment">I’m loving the high Bitcoin profits I’m making since I started with CryptoEdge Literally, the best move I have ever made</span>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    <em class="flag-01"></em>
                                    <span class="name">Dewitt Parker</span>
                                    <span class="comment">“Bitcoin Pro is the only service I can trust when it comes to such a complicated subject like crypto currencies!”</span>
                                </div>
                                <div class="col-md-3">
                                    <em class="flag-02"></em>
                                    <span class="name">Adam Dame</span>
                                    <span class="comment">I’ve trade so many Crypto services but by far this is the most impressive one.</span>
                                </div>
                                <div class="col-md-3">
                                    <em class="flag-03"></em>
                                    <span class="name">Jenna Morais</span>
                                    <span class="comment">Your support team was very welcoming, helpful. In one day, they showed me how to make $783 in profit and the shocking part… all for free!</span>
                                </div>
                                <div class="col-md-3">
                                    <em class="flag-04"></em>
                                    <span class="name">Robert Caldwell</span>
                                    <span class="comment">I’m loving the high Bitcoin profits I’m making since I started with CryptoEdge Literally, the best move I have ever made</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Left and right controls -->
                <a class="left carousel-control" href="#myCarousel" data-slide="prev">
                    <span class="arrow-left"></span>
                    <span class="sr-only">Previous</span>
                </a>
                <a class="right carousel-control" href="#myCarousel" data-slide="next">
                    <span class="arrow-right"></span>
                    <span class="sr-only">Next</span>
                </a>
            </div>
            </div>
        </div>
        <div class="block_04">
            <div class="container-fluid">
                    <div class="block-image no-gutter">
                        <img class="img-responsive" src="images/bitcoin_bg.jpg" alt="Bitcoin Pro Image" />
                    </div>
                    <div class="block-wrap">
                        <div class="block-content">
                            <h3>High Profiting & Free Crypto System</h3>
                            <p class="first">With the recent increase of global interest in the Cryptocurrency Trading World, new traders and new crypto users, have the complicated task of selecting the correct tools for them. Here at CryptoEdge, we offer all our users free tools to help them profit big in the lucrative age of Cryptocurrencies.</p>
                            <p>Rest assured that all you ever wanted to achieve from Crypto Trading, is at the tip of your fingers. Now you have the opportunity to be one step ahead of even the biggest traders in the world!</p>
                        </div>
                    </div>
            </div>
        </div>
        
        <div class="block_05">
            <div class="container">
                <div class="row text-center">
                    <div class="col-md-12">
                        <h2>The Software In Action</h2>
                        <table class="table table-customized">
                            <thead>
                                <tr>
                                    <th>Expiry Date</th>
                                    <th>Asset</th>
                                    <th>Payout</th>
                                    <th>Results</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>19:57:07</td>
                                    <td>EUR/USD</td>
                                    <td>$180.00</td>
                                    <td><em class="icon-check"></em></td>
                                </tr>
                                <tr>
                                    <td>20:12:54</td>
                                    <td>GOLD</td>
                                    <td>$242.00</td>
                                    <td><em class="icon-check"></em></td>
                                </tr>
                                <tr>
                                    <td>14:51:57</td>
                                    <td>NZD/USD</td>
                                    <td>$643.00</td>
                                    <td><em class="icon-check"></em></td>
                                </tr>
                                <tr>
                                    <td>11:31:05</td>
                                    <td>GOOGLE</td>
                                    <td>$302.00</td>
                                    <td><em class="icon-check"></em></td>
                                </tr>
                                <tr>
                                    <td>11:31:05</td>
                                    <td>USD/CAD</td>
                                    <td>$106.00</td>
                                    <td><em class="icon-check"></em></td>
                                </tr>
                                <tr>
                                    <td>19:57:07</td>
                                    <td>EUR/USD</td>
                                    <td>$180.00</td>
                                    <td><em class="icon-check"></em></td>
                                </tr>
                                <tr>
                                    <td>20:12:54</td>
                                    <td>GOLD</td>
                                    <td>$242.00</td>
                                    <td><em class="icon-check"></em></td>
                                </tr>
                                <tr>
                                    <td>14:51:57</td>
                                    <td>NZD/USD</td>
                                    <td>$643.00</td>
                                    <td><em class="icon-check"></em></td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="block_06">
            <div class="container">
                <h2 class="text-center title-heading">Sign Up Today</h2>
                <div class="row">
                    <form id="step1-form-b" class="signup-form">
                        <div class="col-md-4">
                             <input type="text" name="fullname" placeholder="Name" class="name" required>
                        </div>
                        <div class="col-md-4">
                            <input type="email" name="email" placeholder="Email" class="email">
                        </div>
                        <div class="col-md-4">
                            <input class="prefix" name="prefix" type="text" readonly>
                            <input class="phone" name="phone" type="text" placeholder="Phone" required>
                        </div>
                        <div class="col-md-12">
                            <input class="instant-access btn" name="button_signup" type="submit" value="Instant Access">
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <div class="block_07">
            <div class="container">
                <div class="row">
                    <div class="col-md-3 text-center">
                        <img src="images/icons_01.png" alt="High Speed">
                        <div class="info-block">
                            <span class="title">High Speed</span>
                            <span class="description">By Using Blockchain technology we deliver a 99.7% success rate on average to all our users.</span>
                        </div>
                    </div>
                    <div class="col-md-3 text-center">
                        <img src="images/icons_02.png" alt="High Speed Trading">
                        <div class="info-block">
                            <span class="title">High Speed Trading</span>
                            <span class="description">You enjoy a huge growing menu of Crypto Coins to trade on featuring more Cryptocurrency than any other platform.</span>
                        </div>    
                    </div>
                    <div class="col-md-3 text-center">
                        <img src="images/icons_03.png" alt="User Friendly">
                        <div class="info-block">
                            <span class="title">User Friendly</span>
                            <span class="description">Our easy to use platform caters to the needs of all our users. From beginners to experience Crypto Traders .</span>
                        </div>
                    </div>
                    <div class="col-md-3 text-center">
                        <img src="images/icons_04.png" alt="Easy Access">
                        <div class="info-block">
                            <span class="title">Easy Access</span>
                            <span class="description">A Crypto Trading system with easy access technology from any computer or mobile device. PC, MAC, Android, IOS and others.</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <footer>
            <div class="container">
                <p class="text-center text-white">&copy; 2018 All Right Reserved</p>
            </div>
        </footer>
        <!-- jQuery library -->
        <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
       
        <!-- Latest compiled JavaScript -->
        <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
        
        <!-- jQuery Validate -->
        <script src="https://cdn.jsdelivr.net/npm/jquery-validation@1.17.0/dist/jquery.validate.min.js"></script>
        
        <!--geoplugin -->
        <script src="http://www.geoplugin.net/javascript.gp"></script>
        
        <!-- jQuery libphonenumber -->
        <script src="js/libphonenumber.js"></script>
        
        <!-- Customized JS-->
        <script src="js/main.js"></script>
    </body>
</html>
